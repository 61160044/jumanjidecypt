const draggableElements = document.querySelectorAll(".draggable");
const droppableElements = document.querySelectorAll(".droppable");

draggableElements.forEach(elem => {
  elem.addEventListener("dragstart", dragStart); 
});

droppableElements.forEach(elem => {
  elem.addEventListener("dragenter", dragEnter); 
  elem.addEventListener("dragover", dragOver); 
  elem.addEventListener("dragleave", dragLeave); 
  elem.addEventListener("drop", drop); 
});

function dragStart(event) {
  event.dataTransfer.setData("text", event.target.id); 
}


function dragEnter(event) {
  if (!event.target.classList.contains("dropped")) {
    event.target.classList.add("droppable-hover");
  }
}

function dragOver(event) {
  if (!event.target.classList.contains("dropped")) {
    event.preventDefault(); 
  }
}

function dragLeave(event) {
  if (!event.target.classList.contains("dropped")) {
    event.target.classList.remove("droppable-hover");
  }
}


let score = 0;
function drop(event) {
  event.preventDefault(); 
  event.target.classList.remove("droppable-hover");
  const draggableElementData = event.dataTransfer.getData("text"); 
  const droppableElementData = event.target.getAttribute("data-draggable-id");
  const isCorrectMatching = draggableElementData === droppableElementData;
 
  if (isCorrectMatching) {
    const draggableElement = document.getElementById(draggableElementData);
    event.target.classList.add("dropped");
    event.target.style.backgroundColor = window.getComputedStyle(draggableElement).color;
    draggableElement.classList.add("dragged");
    draggableElement.setAttribute("draggable", "false");
    event.target.insertAdjacentHTML("afterbegin", `<i class="fas fa-${draggableElementData}"></i>`);
    var audio = new Audio('/sound/soundcorrect.mp3');
    audio.play();
    score++;


    if(score == 7){
      console.log("End")
      enablepassbtn();
      var happy = new Audio('/sound/happy.wav');
      happy.play();

      onTimesUp();
      var finished = new Audio('/sound/finished.wav');
      if(timeLeft >= 30){
        document.getElementById("demo").innerHTML = "3";
        finished.play();
      }else if(timeLeft >= 20){
        document.getElementById("demo").innerHTML = "2";
        finished.play();
      }else if(timeLeft > 0 && timeLeft < 20){
        document.getElementById("demo").innerHTML = "1";
        finished.play();
      }else{
        document.getElementById("demo").innerHTML = "0";
      }
    }
  }
}

const TIME_LIMIT = 60;
let timePassed = 0;
let timeLeft = TIME_LIMIT;
let timerInterval = null;


function onTimesUp() {
  clearInterval(timerInterval);
}

function startTimer() {
    disablepassbtn();
    disableplayagainbtn();
    timerInterval = setInterval(() => {
    timePassed = timePassed += 1;
    timeLeft = TIME_LIMIT - timePassed;
    document.getElementById("countdown").innerHTML = formatTime(
      timeLeft
    );

    if (timeLeft === 0) {
      onTimesUp();
      enableplayagainbtn();
      var over = new Audio('/sound/gameover.wav');
      over.play();
    }
  }, 1000);
}

function formatTime(time) {
  const minutes = Math.floor(time / 60);
  let seconds = time % 60;

  if (seconds < 10) {
    seconds = `0${seconds}`;
  }
  return `${minutes}:${seconds}`;
}

function disablepassbtn() {
  document.getElementById("passbutton").style.visibility = 'hidden';
}

function disableplayagainbtn() {
  document.getElementById("againbutton").style.visibility = 'hidden';
}

function enablepassbtn() {
  document.getElementById("passbutton").style.visibility = 'visible';
}

function enableplayagainbtn() {
  document.getElementById("againbutton").style.visibility = 'visible';
}
